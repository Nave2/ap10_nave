#include "Customer.h"

Customer::Customer(std::string name)
{
	this->_name = name;
}

Customer::Customer()
{
	this->_name = "";
}

double Customer::totalSum() const
{
	double sum = 0;
	std::set<Item>::iterator it;
	for (it = this->_items.begin(); it != this->_items.end(); it++)
		sum += (*it).totalPrice();
	return sum;
}

void Customer::addItem(Item item)
{
	std::set<Item>::iterator itemIt = this->_items.find(item);
	if (itemIt == this->_items.end())
		this->_items.insert(item);
	else {
		Item updatedItem(item);
		updatedItem.setCount((*itemIt).getCount() + 1);
		this->_items.erase(item);
		this->_items.insert(updatedItem);
	}
}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator itemIt = this->_items.find(item);
	if (itemIt != this->_items.end()) {
		Item updatedItem(item);
		updatedItem.setCount((*itemIt).getCount() - 1);
		this->_items.erase(item);
		if (updatedItem.getCount() != 0)
			this->_items.insert(updatedItem);
	}
}

std::set<Item> Customer::getItems()
{
	return this->_items;
}
