#include"Customer.h"
#include<map>
#include <iterator>
#include <ostream>
#include <algorithm>

#define NUM_ITEMS 10

int printMenu()
{
	std::cout << "Welcome to MagshiMart!" << std::endl <<
		"1. to sign as customer and buy items" << std::endl <<
		"2. to uptade existing customer's items" << std::endl <<
		"3. to print the customer who pays the most" << std::endl <<
		"4. to exit" << std::endl;
	int option;
	std::cin >> option;
	return option;
}

void printItem(Item item, bool printCount = false)
{
	std::cout << item.getSerialNumber() << ". " << item.getName() << ", price: " << item.getUnitPrice();
	if (printCount)
		std::cout << ", amount: " << item.getCount();
	std::cout << std::endl;
}

void getItemsFromCustomer(Customer& customer, Item itemList[])
{
	int selectedItem = -1;
	while (selectedItem != 0)
	{
		std::cout << "The items you can buy are: (0 to exit)" << std::endl;
		for (int i = 0; i < NUM_ITEMS; i++)
			printItem(itemList[i]);
		std::cout << "What item would you like to buy? Input: ";
		std::cin >> selectedItem;
		if (selectedItem != 0)
			customer.addItem(itemList[selectedItem - 1]);
	}
}

bool cmpCustomers(const std::pair<std::string, Customer> a, const std::pair<std::string, Customer> b)
{
	return (a.second.totalSum() < b.second.totalSum());
}

int main()
{
	std::map<std::string, Customer> abcCustomers;
	Item itemList[NUM_ITEMS] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("chicken","00007",25.99),
		Item("fish", "00008", 31.65),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	int option = printMenu();
	while (option != 4)
	{
		switch (option)
		{
		case 1: {
			std::string name;
			std::cout << "Enter customer name: ";
			std::cin >> name;
			std::map<std::string, Customer>::iterator customerIt = abcCustomers.find(name);
			if (customerIt != abcCustomers.end())
				std::cerr << "Customer with that name already exists!" << std::endl;
			else { // new customer
				Customer customer(name);
				int selectedItem = -1;
				getItemsFromCustomer(customer, itemList);
				abcCustomers[name] = customer;
			}
			break;
		}
		case 2: {
			std::string name;
			std::cout << "Enter customer name: ";
			std::cin >> name;
			std::map<std::string, Customer>::iterator customerIt = abcCustomers.find(name);
			if (customerIt == abcCustomers.end())
			{
				std::cerr << "Such customer doesn't exists." << std::endl;
			}
			else
			{
				//print customer's items list
				std::set<Item> customerItems = abcCustomers[name].getItems();
				std::cout << "customers list: " << std::endl;
				for (std::set<Item>::iterator it = customerItems.begin(); it != customerItems.end(); it++)
					printItem((*it), true);
				std::cout << "options: " << std::endl;
				std::cout << "1. Add items" << std::endl <<
					"2. Remove items" << std::endl <<
					"3. Back to menu" << std::endl;
				int option = 0;
				std::cin >> option;
				switch (option)
				{
				case 1:
					getItemsFromCustomer(abcCustomers[name], itemList);
					break;
				case 2:
				{
					int itemNum = -1;
					while (itemNum != 0)
					{
						std::cout << "Enter item num to delete (0 to exit): ";
						std::cin >> itemNum;
						if (itemNum != 0)
						{
							abcCustomers[name].removeItem(itemList[itemNum - 1]);
						}
					}
					break;
				}
				default:
					break;
				}
			}
			break;
		}
		case 3: {
			std::map<std::string, Customer>::iterator max = std::max_element(abcCustomers.begin(), abcCustomers.end(), cmpCustomers);
			if (max == abcCustomers.end())
				std::cout << "empty customer list :(" << std::endl;
			else
				std::cout << "most frayer customer: " << (*max).first <<
				" who payed " << (*max).second.totalSum() << std::endl;
			break;
		}
		default: {
			break;
		}
		}

		option = printMenu();
	}

	std::cout << "bye bye!" << std::endl;
	return 0;
}